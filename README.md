Hi guys, my name is *PacBuilder*!

![PacBuilder](http://www.archlinux.it/pacbuilder/pacbuilder.png)

PacBuilder is a simple utility which gives the users the ability to compile/recompile archlinux packages, fetching their PKGBUILDs from both ABS and AUR. This software could be considered similar to gentoo's "emerge".

It is written from scratch with a simple but organized code, and supports translations (using gettext) too!

It differs from other tools like yaourt because it has few features like recompiling the whole system and/or build the dependencies of a package (while yaourt simply installs the binary).

Everything is provided with a nice text-based interface.
